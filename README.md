# Syncthing in docker-compose
[I use this VPS hosting](https://www.hostens.com/?affid=1251)

Works together with [jwilder/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy)

`nginx-proxy` project [here](https://gitlab.com/sync-server/nginx-proxy)

# Update of syncthing version
```bash
docker-compose pull
docker-compose up -d
docker system prune # optional, to remove old images
```